package asertopoc.GET.greeting

default allowed = false

allowed {
    some index
    input.user.properties.roles[index] == "admin"
}
